<?php
/*
 * Funções e Definições do Tema
 *
 * Desenvolvido por: Agência 5G
 * URL: https://agencia5g.com.br/
 * Version: 1.0
 * Date: 12-09-2017
 *
 */
$theme = wp_get_theme( '2017-woo-default' );
$version_5g = $theme['Version'];
if ( ! isset( $content_width ) ) {
	$content_width = 980;
}
$init_5g = (object) array(
	'version'    => $version_5g,
	'main'       => require 'inc/class-5g.php',
	'customizer' => require 'inc/class-5g-customizer.php',
	'functions'  => require 'inc/5g-functions.php',
);
/* ----- ALTERAR ABAIXO ------------------------------------------------------------ */
if ( ! function_exists( 'agencia5g_nav_menus' ) ) {
	function agencia5g_nav_menus() {
		$menus = array(
			'primary' => __( 'Primary Menu', 'agencia5g' ),
		);
		return $menus;
	}
}
if ( ! function_exists( 'agencia5g_google_fonts' ) ) {
	function agencia5g_google_fonts() {
		$fonts = array(
			'source-sans-pro' => 'Source+Sans+Pro:400,400italic,700',
		);
		return $fonts;
	}
}
if ( ! function_exists( 'agencia5g_css_sources' ) ) {
	function agencia5g_css_sources() {
		$css = array(
			'5g-bootstrap'         => 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css',
		);
		return $css;
	}
}
if ( ! function_exists( 'agencia5g_js_sources' ) ) {
	function agencia5g_js_sources() {
		$js = array(
			'5g-jquery'    => 'https://code.jquery.com/jquery-3.2.1.slim.min.js',
			'5g-proper'    => 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js',
			'5g-bootstrap' => 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js',
			'5g-font-awesome' => 'https://use.fontawesome.com/90b99fbf0e.js',
		);
		return $js;
	}
}
if ( ! function_exists( 'agencia5g_widgets' ) ) {
	function agencia5g_widgets() {
		$sidebar_args['header-1'] = array(
			'name'          => __( 'Header', 'agencia5g' ),
			'id'            => 'header-1',
			'description'   => 'Descrição do Header'
		);
		/*$sidebar_args['sidebar-1'] = array(
			'name'          => __( 'Sidebar', 'agencia5g' ),
			'id'            => 'sidebar-1',
			'description'   => 'Descrição do Sidebar'
		);
		$sidebar_args['sidebar-2'] = array(
			'name'          => __( 'Sidebar 2', 'agencia5g' ),
			'id'            => 'sidebar-2',
			'description'   => 'Descrição do Sidebar',
			'before_widget' => '<div id="%1$s" class="widget beta %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<span class="beta widget-title">',
			'after_title'   => '</span>',
		);
		/* ---------- Configuração para Todos ---------- */
		$sidebar_args['widget_tags'] = array(
			'before_widget' => '<div id="%1$s" class="widget2 %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<span class="gamma2 widget-title">',
			'after_title'   => '</span>',
		);
		return $sidebar_args;
	}
}
?>