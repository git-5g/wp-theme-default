<?php
/*
 * Funções e Definições do Tema
 *
 * Desenvolvido por: Agência 5G
 * URL: https://agencia5g.com.br/
 * Version: 1.0
 * Date: 12-09-2017
 *
 */
if ( ! function_exists( 'woo_5g_is_woocommerce_activated' ) ) {
    function woo_5g_is_woocommerce_activated() {
        //_deprecated_function( 'is_woocommerce_activated', '2.1.6', 'storefront_is_woocommerce_activated' );
        return class_exists( 'WooCommerce' ) ? true : false;
    }
}
/* ----- Funções para Progressive Web Apps ------------------------------ */
if ( ! function_exists( 'agencia5g_add_meta_tags' ) ) {
    function agencia5g_add_meta_tags() {
        $meta_charset = '<meta charset="'.get_bloginfo( 'charset' ).'">';
        echo __($meta_charset."\n", 'agencia5g');
        $meta_http_equiv = '<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">';
        echo __($meta_http_equiv."\n", 'agencia5g');
        $meta_viewport = '<meta name="viewport" content="width=device-width, initial-scale=1">';
        echo __($meta_viewport."\n", 'agencia5g');
        $meta_theme_color = '<meta name="theme-color" content="'.get_theme_mod( '5g_meta_theme_color' ).'">'; // Chrome, Firefox OS and Opera
        echo __($meta_theme_color."\n", 'agencia5g');
        $meta_apple_title = '<meta name="apple-mobile-web-app-title" content="'.get_bloginfo( 'name' ).'">';// iOS Safari
        echo __($meta_apple_title."\n", 'agencia5g');
        $content=(get_theme_mod( '5g_meta_apple_capable' ))?'yes':'no';
        $meta_apple_capable = '<meta name="apple-mobile-web-app-capable" content="'.$content.'">';// iOS Safari
        echo __($meta_apple_capable."\n", 'agencia5g');
        $meta_apple_bar_style = '<meta name="apple-mobile-web-app-status-bar-style" content="'.get_theme_mod( '5g_meta_status_bar_style' ).'">'; // iOS Safari black or black-translucent
        echo __($meta_apple_bar_style."\n", 'agencia5g');
        $meta_msapp_name = '<meta name="application-name" content="'.get_bloginfo( 'name' ).'" />'; // Windows Phone & >IE10
        echo __($meta_msapp_name."\n", 'agencia5g');
        $meta_msapp_tile_color = '<meta name="msapplication-TileColor" content="'.get_theme_mod( '5g_meta_tile_color' ).'" />'; // Windows Phone & >IE10
        echo __($meta_msapp_tile_color."\n", 'agencia5g');
        if(get_theme_mod( '5g_msapp_meta_wide310_150' )):
            $meta_msapp_wide310_150 = '<meta name="msapplication-wide310x150logo" content="'.wp_get_attachment_image_src(get_theme_mod( '5g_msapp_meta_wide310_150' ),'full','true')[0].'" />'; // Windows Phone & >IE10
            echo __($meta_msapp_wide310_150."\n", 'agencia5g');
        endif;
        if(get_theme_mod( '5g_msapp_meta_wide310_310' )):
            $meta_msapp_wide310_310 = '<meta name="msapplication-square310x310logo" content="'.wp_get_attachment_image_src(get_theme_mod( '5g_msapp_meta_wide310_310' ),'full','true')[0].'" />'; // Windows Phone & >IE10
            echo __($meta_msapp_wide310_310."\n", 'agencia5g');
        endif;
    }
}
if ( ! function_exists( 'agencia5g_inc_manifest_link' ) ) {
    function agencia5g_inc_manifest_link() {
        $jsonFile = get_template_directory().'/manifest.json';
        if(get_theme_mod( '5g_json_enable' ) && has_site_icon()):
            agencia5g_create_manifest_json($jsonFile);
            $manifest_link = '<link rel="manifest" href="'.get_template_directory_uri().'/manifest.json">';
            echo __($manifest_link."\n", 'agencia5g');
        else:
            if(file_exists($jsonFile))
                unlink($jsonFile);
        endif;
    }
}
if ( ! function_exists( 'agencia5g_create_manifest_json' ) ) {
    function agencia5g_create_manifest_json( $jsonFile ) {
        $jsonData = array(
            'name'             => get_bloginfo( 'name' ).' | '.get_bloginfo( 'description' ),
            'short_name'       => get_bloginfo( 'name' ),
            'description'      => get_theme_mod( '5g_json_description' ),
            'icons'            => array(
                array(
                'src'   => get_site_icon_url(128),
                'sizes' => '128x128',
                'type'  => 'image/png'
                ),
                array(
                'src'   => get_site_icon_url(144),
                'sizes' => '144x144',
                'type'  => 'image/png'
                ),
                array(
                'src'   => get_site_icon_url(152),
                'sizes' => '152x152',
                'type'  => 'image/png'
                ),
                array(
                'src'   => get_site_icon_url(192),
                'sizes' => '192x192',
                'type'  => 'image/png'
                ),
                array(
                'src'   => get_site_icon_url(256),
                'sizes' => '256x256',
                'type'  => 'image/png'
                ),
                array(
                'src'   => get_site_icon_url(512),
                'sizes' => '512x512',
                'type'  => 'image/png'
                ),
            ),
            'start_url'        => get_theme_mod( '5g_json_start_url' ),
            "scope"            => get_theme_mod( '5g_json_scope' ),
            'display'          => get_theme_mod( '5g_json_display' ),
            'orientation'      => get_theme_mod( '5g_json_orientation' ),
            'background_color' => get_theme_mod( '5g_json_background_color' ),
            'theme_color'      => get_theme_mod( '5g_meta_theme_color' )
            );
            if(!file_exists($jsonFile)):
                $fp = fopen($jsonFile, 'w');
                fwrite($fp, json_encode( $jsonData ));
                fclose($fp);
            endif;
    }
}
if ( ! function_exists( 'agencia5g_sanitize_apple_meta_status_bar_style' ) ) {
    function agencia5g_sanitize_apple_meta_status_bar_style( $input ) {
        //_deprecated_function( 'storefront_sanitize_layout', '2.0', 'storefront_sanitize_choices' );
        $valid = array(
            'black'             => 'Black',
            'black-translucent' => 'Black Translucent',
        );
        if ( array_key_exists( $input, $valid ) ) {
            return $input;
        } else {
            return '';
        }
    }
}
if ( ! function_exists( 'agencia5g_sanitize_json_display' ) ) {
    function agencia5g_sanitize_json_display( $input ) {
        //_deprecated_function( 'storefront_sanitize_layout', '2.0', 'storefront_sanitize_choices' );
        $valid = array(
            'browser'    => __( 'Browser', 'agencia5g' ),
            'minimal-ui' => __( 'Minimal UI', 'agencia5g' ),
            'standalone' => __( 'Standalone', 'agencia5g' ),
            'fullscreen' => __( 'Fullscreen', 'agencia5g' ),
        );
        if ( array_key_exists( $input, $valid ) ) {
            return $input;
        } else {
            return '';
        }
    }
}
if ( ! function_exists( 'agencia5g_sanitize_json_orientation' ) ) {
    function agencia5g_sanitize_json_orientation( $input ) {
        //_deprecated_function( 'storefront_sanitize_layout', '2.0', 'storefront_sanitize_choices' );
        $valid = array(
            'any'                 => __( 'Any', 'agencia5g' ),
            'natural'             => __( 'Natural', 'agencia5g' ),
            'landscape'           => __( 'Landscape', 'agencia5g' ),
            'landscape-primary'   => __( 'Landscape Primary', 'agencia5g' ),
            'landscape-secondary' => __( 'Landscape Secondary', 'agencia5g' ),
            'portrait'            => __( 'Portrait', 'agencia5g' ),
            'portrait-primary'    => __( 'Portrait Primary', 'agencia5g' ),
            'portrait-secondary'  => __( 'Portrait Secondary', 'agencia5g' ),
        );
        if ( array_key_exists( $input, $valid ) ) {
            return $input;
        } else {
            return '';
        }
    }
}
if ( ! function_exists( 'agencia5g_site_icon_sizes' ) ) {
    function agencia5g_site_icon_sizes( $sizes ) {
        $sizes[] = 128;
        $sizes[] = 144;
        $sizes[] = 152;
        $sizes[] = 256;
        return $sizes;
    }
}
add_filter( 'site_icon_image_sizes', 'agencia5g_site_icon_sizes' );
add_action( 'wp_head', 'agencia5g_add_meta_tags',0 );
add_action( 'wp_head', 'agencia5g_inc_manifest_link',20);
/* ----- Funções para Progressive Web Apps ------------------------------ */
if ( ! function_exists( 'agencia5g_favicon_ico' ) ) {
    function agencia5g_favicon_ico() {
        if(!get_theme_mod( '5g_favicon_ico' ))
            return '';
        $favicon_ico = '<link rel="shortcut icon" href="'.wp_get_attachment_image_src(get_theme_mod( '5g_favicon_ico' ),'full','true')[0].'">'; // ----- ARRUMAR INPUT DO ICO
        echo __($favicon_ico."\n", 'agencia5g');
    }
}
add_action( 'wp_head', 'agencia5g_favicon_ico' , 20);
if ( ! function_exists( 'agencia5g_sanitize_hex_color' ) ) {
    function agencia5g_sanitize_hex_color( $color ) {
        _deprecated_function( 'agencia5g_sanitize_hex_color', '2.0', 'sanitize_hex_color' );
        if ( '' === $color ) {
            return '';
        }
        if ( preg_match( '|^#([A-Fa-f0-9]{3}){1,2}$|', $color ) ) {
            return $color;
        }
        return null;
    }
}
if ( ! function_exists( 'agencia5g_sanitize_checkbox' ) ) {
    function agencia5g_sanitize_checkbox( $checked ) {
        return ( ( isset( $checked ) && true == $checked ) ? true : false );
    }
}
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_generator' );
?>