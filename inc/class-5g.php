<?php
/*
 * Funções e Definições do Tema
 *
 * Desenvolvido por: Agência 5G
 * URL: https://agencia5g.com.br/
 * Version: 1.0
 * Date: 12-09-2017
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( ! class_exists( 'Agencia5G' ) ) :
	class Agencia5G {
		public function __construct() {
			add_action( 'after_setup_theme',  array( $this, 'setup'         )  );
			add_action( 'widgets_init',       array( $this, 'widgets_setup' )  );
			add_action( 'wp_enqueue_scripts', array( $this, 'scripts_setup' ), 10 );
		}
		public function setup() {
			load_theme_textdomain( 'agencia5g', get_template_directory() . '/languages' );
			add_theme_support( 'automatic-feed-links' );
			add_theme_support( 'post-thumbnails' );
			add_theme_support( 'custom-logo', array('flex-height' => true,'flex-width'  => true ) );
			add_theme_support( 'html5', array('search-form','comment-form','comment-list','gallery','caption','widgets',) );
			add_theme_support( 'post-formats', array('aside','image','video','quote','link','gallery','audio',) );
			add_theme_support( 'title-tag' );
			add_theme_support( 'customize-selective-refresh-widgets' );
			register_nav_menus( agencia5g_nav_menus() );
		}
		public function widgets_setup() {
			$sidebar_args = apply_filters( '5g_sidebar_args', agencia5g_widgets() );
			foreach ( $sidebar_args as $sidebar => $args ) {
				$filter_hook = sprintf( '5g_%s_widget_tags', $sidebar );
				if(count($args)===3):
					$widget_tags = apply_filters( $filter_hook, $sidebar_args['widget_tags'] );
				else:
					if(!array_key_exists('before_widget', $args) && !array_key_exists('after_widget', $args)):
						$widget_tags = array(
							'before_widget' => $sidebar_args['widget_tags']['before_widget'],
							'after_widget'  => $sidebar_args['widget_tags']['after_widget'],
						);
						$widget_tags = apply_filters( $filter_hook,$widget_tags);
					endif;
					if(!array_key_exists('before_title', $args) && !array_key_exists('after_title', $args)):
						$widget_tags = array(
							'before_title' => $sidebar_args['widget_tags']['before_title'],
							'after_title'  => $sidebar_args['widget_tags']['after_title'],
						);
						$widget_tags = apply_filters( $filter_hook,$widget_tags);
					endif;
				endif;
				if ( is_array( $widget_tags ) ) {
					register_sidebar( $args + $widget_tags );
				}
			}
		}
		public function scripts_setup() {
			global $version_5g;
			$script_args = apply_filters( '5g_script_args', agencia5g_css_sources() );
			foreach ( $script_args as $script => $args ) {
				wp_enqueue_style( $script, $args,array(),$version_5g);
			}
			$font_families = apply_filters( '5g_google_font_families', agencia5g_google_fonts() );
			$google_fonts = array(
				'family' => implode( '|', $font_families ),
				//'subset' => urlencode( 'latin,latin-ext' ),
			);
			wp_enqueue_style( '5g-google-fonts', add_query_arg( $google_fonts, 'https://fonts.googleapis.com/css' ),'', $version_5g);
 			wp_enqueue_style( '5g-style', get_stylesheet_uri(),'', $version_5g);
 			wp_style_add_data( '5g-style', 'rtl', 'replace' );
        	wp_enqueue_script( '5g-html5shiv','https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js',false,'3.7.3',false);
        	wp_script_add_data( '5g-html5shiv', 'conditional', 'lt IE 9' );
        	wp_enqueue_script( '5g-respond', 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js',false,'1.4.2',false);
        	wp_script_add_data( '5g-respond', 'conditional', 'lt IE 9' );
			$js_args = apply_filters( '5g_js_args', agencia5g_js_sources() );
			foreach ( $js_args as $script => $args ) {
				wp_enqueue_script( $script, $args,false,$version_5g,true);
			}
			wp_enqueue_script( '5g-script', get_template_directory_uri().'/assets/js/scripts.js',false,$version_5g,true);
		}
	}
endif;
return new Agencia5G();