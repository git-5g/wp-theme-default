<?php
/*
 * Funções e Definições do Tema
 *
 * Desenvolvido por: Agência 5G
 * URL: https://agencia5g.com.br/
 * Version: 1.0
 * Date: 12-09-2017
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( ! class_exists( 'Agencia5G_Customizer' ) ) :
	class Agencia5G_Customizer {
		public function __construct() {
			add_action( 'after_setup_theme', array( $this, 'custom_setup'  ) );
			add_action( 'widgets_init',      array( $this, 'custom_widgets_setup' ) );
			add_action( 'customize_register',array( $this, 'custom_register' ), 10 );
			add_action( 'customize_register',array( $this, 'edit_default_customizer_settings' ), 99 );
			add_action( 'init',              array( $this, 'default_theme_mod_values' ), 10 );
		}
		public static function get_5g_default_setting_values() {
			return apply_filters( '5g_setting_default_values', $args = array(
				'5g_meta_theme_color'      => '#ffffff',
				'5g_meta_tile_color'       => '#ffffff',
				'5g_meta_apple_capable'    => false,
				'5g_meta_status_bar_style' => 'black',
				'5g_json_start_url'        => '/?utm_source=homescreen',
				'5g_json_enable'           => false,
				'5g_json_description'      => '',
				'5g_json_display'          => 'standalone',
				'5g_json_orientation'      => 'portrait',
				'5g_json_background_color' => '#ffffff',
				'5g_json_scope'            => '/',
			) );
		}
		public function default_theme_mod_values() {
			foreach ( self::get_5g_default_setting_values() as $mod => $val ) {
				add_filter( 'theme_mod_' . $mod, array( $this, 'get_theme_mod_value' ), 10 );
			}
		}
		public function get_theme_mod_value( $value ) {
			$key = substr( current_filter(), 10 );
			$set_theme_mods = get_theme_mods();
			if ( isset( $set_theme_mods[ $key ] ) ) {
				return $value;
			}
			$values = $this->get_5g_default_setting_values();
			return isset( $values[ $key ] ) ? $values[ $key ] : $value;
		}
		public function edit_default_customizer_settings( $wp_customize ) {
			foreach ( self::get_5g_default_setting_values() as $mod => $val ) {
				$wp_customize->get_setting( $mod )->default = $val;
			}
		}
		public function custom_setup() {
		}
		public function custom_widgets_setup() {
		}
		public function custom_register( $wp_customize ) {
			$wp_customize->add_setting('5g_favicon_ico');
	        $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, '5g_favicon_ico', array(
            	'priority'    => 100,
            	'label'       => 'Favicon ICO',
            	'description' => 'O ícone do site é utilizado como ícone em navegadores e aplicativos para o seu site. Ícones devem ser quadrados com 16 píxeis de largura e altura.',
            	'section'     => 'title_tagline',
            	'settings'    => '5g_favicon_ico',
            	'mime_type'   => array('image/x-icon'),
	        )));
			$wp_customize->add_panel( '5g_painel_mobile_config', array(
			 	'priority'    => 30,
			 	'title'       => __('Mobile Meta Tags & Apps', 'agencia5g'),
			 	'description' => __('Configurações de Meta Tags exclusivas para Navegadores em dispositivos Móveis.', 'agencia5g'),
			));
			/* Android */
			$wp_customize->add_section( '5g_android_meta_tag', array(
				'priority'    => 20,
				'title'       => __( 'Android', 'agencia5g' ),
				'description' => 'Configurações de Meta Tags exclusivas para Android.',
				'panel'       => '5g_painel_mobile_config',
			) );
			$wp_customize->add_setting( '5g_meta_theme_color', array(
				'default'           => apply_filters( '5g_default_meta_theme_color', '#ffffff' ),
				'sanitize_callback' => 'sanitize_hex_color',
			) );
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, '5g_meta_theme_color', array(
				'label'	   => __( 'Meta Theme Color', 'agencia5g' ),
				'section'  => '5g_android_meta_tag',
				'settings' => '5g_meta_theme_color',
				'priority' => 30,
			) ) );
			/* iOS */
			$wp_customize->add_section( '5g_apple_meta_tag', array(
				'priority'    => 20,
				'title'       => __( 'iOS', 'agencia5g' ),
				'description' => 'Configurações de Meta Tags exclusivas para iOS.',
				'panel'       => '5g_painel_mobile_config',
			) );
			$wp_customize->add_setting( '5g_meta_apple_capable', array(
				'default'           => apply_filters( '5g_default_meta_apple_capable', 'false' ),
				'sanitize_callback' => 'agencia5g_sanitize_checkbox',
			) );
			$wp_customize->add_control( '5g_meta_apple_capable', array(
				'label'       => __( 'Ocultando os componentes da interface de usuário do Safari.', 'agencia5g' ),
				'description' => __( 'Define se um aplicativo da Web é executado no modo de tela cheia. Meta Mobile Web App Capable (apple-mobile-web-app-capable).', 'agencia5g' ),
				'section'     => '5g_apple_meta_tag',
				'priority'    => 30,
				'type'        => 'checkbox',
				'settings'    => '5g_meta_apple_capable',
			) );
			$wp_customize->add_setting( '5g_meta_status_bar_style', array(
				'default'           => 'black',
				'sanitize_callback' => 'agencia5g_sanitize_apple_meta_status_bar_style',
			) );
			$wp_customize->add_control( '5g_meta_status_bar_style', array(
				'label'       => __( 'Alterar a aparência da barra de status.', 'agencia5g' ),
				'description' => __( 'Define o estilo da barra de status para uma aplicação web. Meta Mobile Web Status Bar Style (apple-mobile-web-app-status-bar-style).', 'agencia5g' ),
				'section'     => '5g_apple_meta_tag',
				'priority'    => 31,
				'type'        => 'radio',
				'choices'     => array(
					'black'             => __( 'Black', 'agencia5g' ),
					'black-translucent' => __( 'Black Translucent', 'agencia5g' ),
				),
				'settings'    => '5g_meta_status_bar_style',
			) );
			/* Microsoft */
			$wp_customize->add_section( '5g_microsoft_meta_tag', array(
				'priority'    => 20,
				'title'       => __( 'Microsoft', 'agencia5g' ),
				'description' => 'Configurações de Meta Tags exclusivas para Microsoft.',
				'panel'       => '5g_painel_mobile_config',
			) );
			$wp_customize->add_setting( '5g_meta_tile_color', array(
				'default'           => apply_filters( '5g_default_meta_tile_color', '#ffffff' ),
				'sanitize_callback' => 'sanitize_hex_color',
			) );
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, '5g_meta_tile_color', array(
				'label'	   => __( 'Meta TileColor', 'agencia5g' ),
				'description' => 'Define a cor de fundo para o mosaico.',
				'section'  => '5g_microsoft_meta_tag',
				'settings' => '5g_meta_tile_color',
				'priority' => 30,
			) ) );
			$wp_customize->add_setting('5g_msapp_meta_wide310_150');
	        $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, '5g_msapp_meta_wide310_150', array(
            	'priority'    => 100,
            	'label'       => 'Wide Logo',
            	'description' => 'Definir a imagem para o mosaico largo. Utilize imagem 310x150 ou 558x270 píxeis de largura e altura.',
            	'section'     => '5g_microsoft_meta_tag',
            	'settings'    => '5g_msapp_meta_wide310_150',
            	'mime_type' => array('image/png'),
	        )));
	        $wp_customize->add_setting('5g_msapp_meta_wide310_310');
	        $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, '5g_msapp_meta_wide310_310', array(
            	'priority'    => 100,
            	'label'       => 'Large Logo',
            	'description' => 'Definir a imagem para o mosaico grande. Utilize imagem 310x310 ou 558x558 píxeis de largura e altura.',
            	'section'     => '5g_microsoft_meta_tag',
            	'settings'    => '5g_msapp_meta_wide310_310',
            	'mime_type' => array('image/png'),
	        )));
	        /* Web App Manifest */
			$wp_customize->add_section( '5g_android_web_app', array(
				'priority'    => 20,
				'title'       => __( 'Web App Manifest', 'agencia5g' ),
				'description' => 'Fornecem a capacidade de salvar um site marcado como favorito na tela inicial de um dispositivo.',
				'panel'       => '5g_painel_mobile_config',
			) );
			$wp_customize->add_setting( '5g_json_enable', array(
				'default'           => apply_filters( '5g_default_json_enable', 'false' ),
				'sanitize_callback' => 'agencia5g_sanitize_checkbox',
			) );
			$wp_customize->add_control( '5g_json_enable', array(
				'label'       => __( 'Hablita o Web App Manifest', 'agencia5g' ),
				'description' => __( 'Define se o aplicativo da Web será habilitado para gerar o arquivo manifest.json na pasta do tema do site.', 'agencia5g' ),
				'section'     => '5g_android_web_app',
				'priority'    => 30,
				'type'        => 'checkbox',
				'settings'    => '5g_json_enable',
			) );
			$wp_customize->add_setting( '5g_json_description');
			$wp_customize->add_control( '5g_json_description', array(
				'label'       => __( 'Description', 'agencia5g' ),
				'description' => __( 'Fornece uma descrição geral do que a aplicação faz.', 'agencia5g' ),
				'section'     => '5g_android_web_app',
				'priority'    => 30,
				'type'        => 'textarea',
				'settings'    => '5g_json_description',
			) );
			$wp_customize->add_setting( '5g_json_background_color', array(
				'default'           => apply_filters( '5g_default_json_background_color', '#ffffff' ),
				'sanitize_callback' => 'sanitize_hex_color',
			) );
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, '5g_json_background_color', array(
				'label'	   => __( 'Background Color', 'agencia5g' ),
				'description' => __( 'O Chrome usa essa cor no exato momento em que o aplicativo web é inicializado, e a cor permanece na tela até a primeira renderização do aplicativo.', 'agencia5g' ),
				'section'  => '5g_android_web_app',
				'settings' => '5g_json_background_color',
				'priority' => 30,
			) ) );
			$wp_customize->add_setting( '5g_json_start_url');
			$wp_customize->add_control( '5g_json_start_url', array(
				'label'       => __( 'Start URL', 'agencia5g' ),
				'description' => __( 'Especifica a URL que é carregado quando um usuário executa o aplicativo a partir de um dispositivo. Ex: /?utm_source=homescreen', 'agencia5g' ),
				'section'     => '5g_android_web_app',
				'priority'    => 30,
				'type'        => 'text',
				'settings'    => '5g_json_start_url',
			) );
			$wp_customize->add_setting( '5g_json_scope');
			$wp_customize->add_control( '5g_json_scope', array(
				'label'       => __( 'Scope', 'agencia5g' ),
				'description' => __( 'Se o usuário navega no aplicativo fora do escopo, ele retorna a ser uma página da Web normal. Ex: "scope": "/myapp/".', 'agencia5g' ),
				'section'     => '5g_android_web_app',
				'priority'    => 30,
				'type'        => 'text',
				'settings'    => '5g_json_scope',
			) );
			$wp_customize->add_setting( '5g_json_display', array(
				'default'           => 'standalone',
				'sanitize_callback' => 'agencia5g_sanitize_json_display',
			) );
			$wp_customize->add_control( '5g_json_display', array(
				'label'       => __( 'Display', 'agencia5g' ),
				'description' => __( 'Define o modo de exibição preferido do desenvolvedor para a aplicação.', 'agencia5g' ),
				'section'     => '5g_android_web_app',
				'priority'    => 30,
				'type'        => 'select',
				'choices'     => array(
					'browser'    => __( 'Browser', 'agencia5g' ),
					'minimal-ui' => __( 'Minimal UI', 'agencia5g' ),
					'standalone' => __( 'Standalone', 'agencia5g' ),
					'fullscreen' => __( 'Fullscreen', 'agencia5g' ),
				),
				'settings'    => '5g_json_display',
			) );
			$wp_customize->add_setting( '5g_json_orientation', array(
				'default'           => 'portrait',
				'sanitize_callback' => 'agencia5g_sanitize_json_orientation',
			) );
			$wp_customize->add_control( '5g_json_orientation', array(
				'label'       => __( 'Orientation', 'agencia5g' ),
				'description' => __( 'Define a orientação padrão para todos os contextos de navegação de nível superior da aplicação web.', 'agencia5g' ),
				'section'     => '5g_android_web_app',
				'priority'    => 30,
				'type'        => 'select',
				'choices'     => array(
					'any'                 => __( 'Any', 'agencia5g' ),
					'natural'             => __( 'Natural', 'agencia5g' ),
					'landscape'           => __( 'Landscape', 'agencia5g' ),
					'landscape-primary'   => __( 'Landscape Primary', 'agencia5g' ),
					'landscape-secondary' => __( 'Landscape Secondary', 'agencia5g' ),
					'portrait'            => __( 'Portrait', 'agencia5g' ),
					'portrait-primary'    => __( 'Portrait Primary', 'agencia5g' ),
					'portrait-secondary'  => __( 'Portrait Secondary', 'agencia5g' ),
				),
				'settings'    => '5g_json_orientation',
			) );
		}
	}
endif;
return new Agencia5G_Customizer();